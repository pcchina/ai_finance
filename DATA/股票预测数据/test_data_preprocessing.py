# !usr/bin env python
# -*- coding: utf-8 -*-

import os
import pandas as pd
import numpy as np

# file_path = '/home/jianjiang/workingdir/CHA/linear-regression/data/ASHAREEARNINGEST_RespectiveStock/000001.SZ.csv'
# file_out_path = '/home/jianjiang/workingdir/CHA/linear-regression/data/test/file_000001.SZ.csv'
# label_path = '/home/jianjiang/workingdir/CHA/linear-regression/data/ASHARESTOCKRATING_RespectiveStock/000001.SZ.csv'
# label_out_path = '/home/jianjiang/workingdir/CHA/linear-regression/data/test/label_000001.SZ.csv'
# #
# files = pd.read_csv(file_path)
# labels = pd.read_csv(label_path)

# files = files.sort_values(by='EST_DT')
# labels = labels.sort_values(by='S_EST_ESTNEWTIME_INST')
# print len(files)
# print len(labels)
# files.to_csv(file_out_path, index=False)
# labels.to_csv(label_out_path, index=False)
# files = pd.concat([files, pd.DataFrame(columns=['S_EST_SCORERATING_INST', 'S_EST_RATING_INST'])])
#
# print files.loc[0, ['RESEARCH_INST_NAME', 'EST_DT']].values


# file_path = '/home/jianjiang/workingdir/CHA/linear-regression/data/ASHAREEARNINGEST.csv'
# label_path = '/home/jianjiang/workingdir/CHA/linear-regression/data/ASHARESTOCKRATING.csv'

# path = '/home/jianjiang/workingdir/CHA/linear-regression/data/ASHAREEARNINGEST_RespectiveStock'
# file_list = os.listdir(path)
# #
# path_label = '/home/jianjiang/workingdir/CHA/linear-regression/data/ASHARESTOCKRATING_RespectiveStock'
# # label_list = os.listdir(path_label)
# outpath = '/home/jianjiang/workingdir/CHA/linear-regression/data/ASHAREEARNINGEST_RespectiveStock_merge'
#
# ed_list = os.listdir(outpath)
#
# for k in file_list:
#     if k in ed_list:
#         continue
#     file_path = os.path.join(path, k)
#     label_path = os.path.join(path_label, k)
#     print k
#
#     files = pd.read_csv(file_path)
#     labels = pd.read_csv(label_path)
#     files = pd.concat([files, pd.DataFrame(columns=['S_EST_SCORERATING_INST', 'S_EST_RATING_INST'])])
#     # print files.head()
#     # print labels.head()
#     for i in xrange(len(files)):
#         for j in xrange(len(labels)):
#             if files.loc[i, ['S_INFO_WINDCODE', 'RESEARCH_INST_NAME', 'EST_DT']].values.all() == labels.loc[j, ['S_INFO_WINDCODE', 'S_EST_INSTITUTE', 'S_EST_ESTNEWTIME_INST']].values.all():
#                 files.loc[i, ['S_EST_SCORERATING_INST', 'S_EST_RATING_INST']] = labels.loc[j, ['S_EST_SCORERATING_INST', 'S_EST_RATING_INST']].values
#
#     files.to_csv(os.path.join(outpath, k))


# clean_file = pd.read_csv(outpath)
# print clean_file.head()
# df = clean_file[['S_INFO_WINDCODE', 'EST_DT', 'REPORTING_PERIOD', 'S_EST_ENDDATE',
#                  'RESEARCH_INST_NAME', 'ANALYST_NAME', 'S_EST_SCORERATING_INST',
#                  'S_EST_RATING_INST', 'EST_BASE_CAP', 'EST_NET_PROFIT', 'EST_EPS_DILUTED',
#                  'EST_MAIN_BUS_INC', 'S_EST_BPS', 'S_EST_DIVIDENDYIELD', 'S_EST_DPS',
#                  'S_EST_EBT', 'S_EST_EPSCAL', 'S_EST_NPCAL', 'S_EST_NPRATE',
#                  'S_EST_PB', 'S_EST_PE', 'S_EST_ROA', 'S_EST_ROE', 'S_EST_OPROFIT']]
#
# outpaths = '/home/jianjiang/workingdir/CHA/linear-regression/data/clean_ASHAREEARNINGEST_labels.csv'
# df.to_csv(outpaths)
# pd.merge()

# file_path = 'FILL_VALUE_PRICE_1.xlsx'
# out_path = 'FILL_VALUE_PRICE_OUT_1.xlsx'
file_path = 'D:/work/20180701/求一段日期中的股价最大值_1.xlsx'
out_path = 'D:/work/20180701/求一段日期中的股价最大值_2.xlsx'
df = pd.read_excel(file_path, sheet_name='Sheet1')
# df = df.head()
industrys = df['CODE'].values
date1 = df['DATE1'].values
date2 = df['DATE2'].values

# price_path = 'ASHAREEODPRICE.csv'
price_path = 'D:\工作\CTBRI\金融AI\数据集\数据库导出数据\修改表头后的股价.csv'
f = open(price_path)
df_price = pd.read_csv(f)

price_mean = []
price_max = []
price_min = []
for i in range(len(industrys)):
    t = df_price.NUM == industrys[i]
    a = df_price[t][df_price.DATE >= date1[i]]
    a = a[a.DATE <= date2[i]]
    price_mean.append(a['Q5.1'].mean())
    price_max.append(a['Q5.1'].max())
    price_min.append(a['Q5.1'].min())
    print('这是第' + str(i) + '行股票信息')
    print('---------------------------------------------')

df['price_mean'] = price_mean
df['price_min'] = price_min
df['price_max'] = price_max
# print(df.head())
df.to_excel(out_path, index=False)
# print factordatafillna.head()
