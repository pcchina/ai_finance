# !usr/bin env python
# -*- coding: utf-8 -*-

from __future__ import division
import numpy as np
import pandas as pd

data_path = 'D:\data\earnings\modify_earnings_2018年6月25日.xlsx'
result_out = "D:\data\earnings\modify_earnings_2018年6月25日_1.xlsx"

raw_df = pd.read_excel(data_path)
modify_pred = raw_df['pred_price'] * (1 - raw_df['diff'])
print(modify_pred)
raw_df['modify_pred'] = modify_pred
print(raw_df.head())
original_earnings = np.array([None] * len(raw_df))
original_earnings[raw_df.ratio <= 0] = \
    raw_df[raw_df.ratio <= 0]['pred_price'] - raw_df[raw_df.ratio <= 0]['begin_price']
original_earnings[raw_df.ratio > 0] = \
    raw_df[raw_df.ratio > 0]['end_price'] - raw_df[raw_df.ratio > 0]['begin_price']

modify_earnings = np.array([None] * len(raw_df))
modify_earnings[raw_df.modify_pred <= raw_df.high_price] = \
    raw_df[raw_df.modify_pred <= raw_df.high_price]['modify_pred'] - raw_df[raw_df.modify_pred <= raw_df.high_price][
        'begin_price']
modify_earnings[raw_df.modify_pred > raw_df.high_price] = \
    raw_df[raw_df.modify_pred > raw_df.high_price]['end_price'] - raw_df[raw_df.modify_pred > raw_df.high_price][
        'begin_price']

print(raw_df.head())

print('modify_earnings', modify_earnings.sum())
print('original_earnings', original_earnings.sum())
raw_df['original_earnings'] = original_earnings
raw_df['modify_earnings'] = modify_earnings
raw_df.to_excel(result_out, index=False)
